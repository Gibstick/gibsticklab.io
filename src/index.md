# Charlie Wang

I’m a software developer. I enjoy banging on keys, whether they’re WASD keys or black and white keys.

Contact: mail at charliewang dot io

[GitHub](https://github.com/gibstick)

# Links
- [Is the Computer Science Club office open?](http://csclub.uwaterloo.ca/~s455wang/office-status/)
